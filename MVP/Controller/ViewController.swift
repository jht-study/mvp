//
//  ViewController.swift
//  MVP
//
//  Created by Administrador on 23/11/2018.
//  Copyright © 2018 JHT. All rights reserved.
//

import UIKit

class ViewController: UIViewController, GreatingViewProtocol {

    var presenter : GreetingViewPresenter?
    
    @IBOutlet weak var showGreetingButton: UIButton!
    @IBOutlet weak var greetingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let model = Person(firtName: "Jeferson", lastName: "Takumi")
        presenter = GreetingViewPresenter(view: self, person: model)
        
        self.showGreetingButton.addTarget(self, action: #selector(didTapButton(_:)), for: .touchUpInside)
    }

    @objc func didTapButton(_ button: UIButton) {
        self.presenter?.showGreeting()
    }
    
    func setGreeting(greeting: String) {
        self.greetingLabel.text = greeting;
    }
}

