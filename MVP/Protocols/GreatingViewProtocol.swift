//
//  GreatingViewProtocol.swift
//  MVP
//
//  Created by Administrador on 23/11/2018.
//  Copyright © 2018 JHT. All rights reserved.
//

protocol GreatingViewProtocol : class {
    func setGreeting(greeting: String)
}
