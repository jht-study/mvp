//
//  GreetingPresenter.swift
//  MVP
//
//  Created by Administrador on 23/11/2018.
//  Copyright © 2018 JHT. All rights reserved.
//

class GreetingViewPresenter : GreetingViewPresenterProtocol{
    
    unowned let view: GreatingViewProtocol
    let person : Person
    
    required init(view: GreatingViewProtocol, person: Person) {
        self.view = view
        self.person = person
    }
    
    func showGreeting() {
        let greeting = "Hello " + self.person.firtName + " " + self.person.lastName
        self.view.setGreeting(greeting: greeting)
    }
}
